//alert("B249");

//IN jS,classes can be created using the "class" keyword and {}.
//Naming convention for classes: Begin with Uppercase characters


/*
	Syntax:

		class <Name> {
	
		}

*/

// class Student {
// 	constructor(name, email) {
		//propertyName = value; // this.name123 = name; any property name as long as same value
// 		this.name = name;
// 		this.email = email;

// 	}
// }

//Instantiation - process of creating objects from class 
//To create an object from a class, use the "new" keyword, when a class has a constructor, we need to supply ALL the value needed by the constructor
//let studentOne = new Student('john', 'john@mail.com');
//console.log(studentOne);

/*
	Mini-Exercise:


*/

class Person {
	constructor(name, age, nationality, address){
		this.name = name;
		this.age = age;
		this.nationality = nationality;
		this.address = address;
		if (typeof age === "number" && age >= 18){
			this.age = age;
		}else {
			this.age = undefined;
		}
	}
}

let person1 = new Person("Dix", 28, "Pilipino", "Laguna");
let person2 = new Person("Boy", 17, "Spanish", "San Pedro");


//Activity

/*
1.CLasses
2.Uppercase first character
3.new
4.instantiate / instantiation
5.constructor

*/

class Student {
	constructor(name, email, grades) {
		this.name = name;
		this.email = email;
		//add this property to the constructor
		this.passedWithHonors = undefined;
		this.passed = undefined;
		this.gradeAve = undefined;
		if(grades.length === 4){
			if(grades.every(grade => grade <= 100 && grade >= 0)){
				this.grades = grades;
			}else {
				this.grades = undefined;
			}
		}else {
			this.grades = undefined;
		}
		// if (typeof grades === "number" && grades.length === 4 && grades >= 0 && grades >= 100 ){
		// 	this.grades = grades;
		// }else {
		// 	this.grades = undefined;
		// }

		//NOTE that no need to use comma when using classes and methods
		//studentFour.login().listGrades().logout();
		//SAMPLE of method chaining
	}
	login(){
		console.log(`${this.email} has logged in`);
		return this; // NOTE this pertains to the object
	}
	logout(){
		console.log(`${this.email} has logged out`);
		return this;
	}
	listGrades(){
		console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`);
		return this;
	}
	computeAve() {
		let sum = 0;
		this.grades.forEach(grade => sum = sum + grade);
		//update property
		this.gradeAve = sum/4;
		//return object
		return this;
		//return sum/4;
	}
	willPass(){
		this.passed = this.gradeAve >= 85 ? true : false;
		return this;
	}

	willPassWithHonors(){
		// if(this.passed) {
		// 	if(this.gradeAve >= 90) {
		// 		return true;
		// 	}else {
		// 		return false;
		// 	}
		// }else {
		// 	return this;
		// }
		this.passedWithHonors = this.gradeAve >= 90 ? true : false;
		return this;
	}
}

//getter and setter
//Best practice dictates that we regulate access to such properties. We do so, via the use of "getters"(regulates the retrieval) and "setter"(regulates manipulation).

//Method Chaining~
/*

*/
let studentOne = new Student("John", "john@mail.com", [89, 84, 78, 88]);
let studentTwo = new Student("Joe", "joe@mail.com", [78, 82, 79, 85]);
let studentThree = new Student("Jane", "jane@mail.com", [87, 89, 91, 93]);
let studentFour = new Student("Jessie", "jessie@mail.com", [91, 89, 92, 93]);


// ACTIVITY 2

/*
1.No
2.No
3.Yes
4.getters
5.return this
*/
